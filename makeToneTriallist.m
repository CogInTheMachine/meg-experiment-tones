function [TrialListNames,TrialList] = makeToneTriallist

global NtoneStimuli;
global MaxTimeBetweenTrials;
global MinTimeBetweenTrials;
global MinTimeBetweenSequences;
global MaxTimeBetweenSequences;
global nSequences;


% Triallist

TrialListNames = {'Exemplar' 'NOTUSED' 'NOTUSED' 'ISI' 'Onset' 'Onset (ABS)' 'Offset (ABS)'....
    'NOTUSED' 'Reaction Time' 'Trigger'};

%Column 1: Exemplar
%Column 2: NOT USED
%Column 3: NOT USED
%Column 4: Interstimulus time (ISI);
%Column 5: Stimulus onset time
%Column 6: Stimulus onset time (absolute)
%Column 7: Stimulus offset time (absolute)
%Column 8: NOT USED
%Column 9: NOT USED
%Column 10: Triggers

% start my generating the order of tones going up and going down sequences

TempGoingUpGoingDown = [zeros(nSequences/2,1) + 1 ; ones(nSequences/2,1) * -1]; 
TempGoingUpGoingDown = TempGoingUpGoingDown(randperm(nSequences,nSequences))';
global sequenceOrder; sequenceOrder = TempGoingUpGoingDown;

%TempExemplarList = repmat(1:NtoneStimuli,[1 NtrialsPerExemplar]);


%%


OrderedSets = arrayfun(@(x) [NtoneStimuli:x:1 ; 1:x:NtoneStimuli],...
    TempGoingUpGoingDown','UniformOutput',false);

for i = 1 : nSequences
    ISItimes{1,i} = MinTimeBetweenTrials+(MaxTimeBetweenTrials-MinTimeBetweenTrials)*rand([NtoneStimuli 1 ]);
    ISItimes{1,i}(end) = MinTimeBetweenSequences + (MaxTimeBetweenSequences - MinTimeBetweenSequences)*rand(1);
end



ISItimes = ISItimes';
ISItimes = cellfun(@(x) x', ISItimes,'UniformOutput',false);



TempExemplarList = [];
TempISIList = [];

for i = 1 : size(OrderedSets,1)
    TempISIList = horzcat(TempISIList,ISItimes{i}); 
    TempExemplarList = horzcat(TempExemplarList,OrderedSets{i});
end
    
    
%TrajectoryDim = ceil(ExemplarList/5)';


% create the trial list
TrialList = zeros(nSequences * NtoneStimuli,8);

% fill in the stuff
TempExemplarList = TempExemplarList';
TrialList(:,1) = TempExemplarList;
TrialList(:,4) = TempISIList';