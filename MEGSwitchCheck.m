function MEGSwitchCheck( IsMEGExpt )
if ~IsMEGExpt
    NoMEGContinue = questdlg('Are you sure this is not a MEG Experiment?', ...
     'MEG Warning', ...
     'Yes, I''m sure. Please continue','No, get me out of here','Yes, I''m sure. Please continue');
    % Handle response
    switch NoMEGContinue
        case 'Yes, I''m sure. Please continue'
            % do nothing and continue;
        case 'No, get me out of here'
            error('Change the IsMEGExpt switch to 1')
        end
end
