
 WaitSecs(10);

INFO = h5info('highrange.h5');

notes = {INFO.Groups.Groups.Datasets.Name};


main = '/samples/v080/';

for i = 1 : length(notes)
    highrange.v080.(notes{i}) = h5read('highrange.h5',[main,notes{i}]);
end

%%

INFO = h5info('lowrange.h5');

notes = {INFO.Groups.Groups.Datasets.Name};


main = '/samples/v080/';

for i = 1 : length(notes)
    lowrange.v080.(notes{i}) = h5read('lowrange.h5',[main,notes{i}]);
end

AudioChannels = 2; % change this to 2 once I've generated new samples   
AudioSampleRate = 44100;

InitializePsychSound;

AudioHandle = PsychPortAudio('Open', [], [], [], AudioSampleRate, AudioChannels);  
PsychPortAudio('Volume', AudioHandle, 1); 

% 
% 
% notes = fieldnames(highrange.v080);
% 
% disp('high range:');
% for i = 1 :  length(notes)
%     WaitSecs(10);
%     sample = highrange.v080.(notes{i});
%     %disp(['Now playing... n',num2str(i)']);
%     PsychPortAudio('FillBuffer', AudioHandle, sample');
%     StimulusOnsetTime = PsychPortAudio('Start', AudioHandle, 1, 0, 1);
%     %disp('done!');
% end


notes = fieldnames(lowrange.v080);
disp('low range:');
for i = 1 :  length(notes)
    WaitSecs(10);
    sample = lowrange.v080.(notes{i});
    %disp(['Now playing... n',num2str(i)']);
    PsychPortAudio('FillBuffer', AudioHandle, sample');
    StimulusOnsetTime = PsychPortAudio('Start', AudioHandle, 1, 0, 1);
    %disp('done!');
end

PsychPortAudio('close');