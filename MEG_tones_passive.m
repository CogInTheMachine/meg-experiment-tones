
clear all;
startTimeMark = clock;
blockScreen = 0;
%% initalize the sound system
AudioChannels = 2; 
AudioSampleRate = 44100;

InitializePsychSound;

AudioHandle = PsychPortAudio('Open', [], [], [], AudioSampleRate, AudioChannels);  
PsychPortAudio('Volume', AudioHandle, 1) 



IsMEGExpt = 1; % set whether it is MEG or not

pulse_width = .005; % set trigger pulse width
FixSize = 16;
gammaVals=[2 2 2];
StimDuration = .500;
ResponseTimeOut = 1;
ValidResponses = {'y' 'b' 'q'};
FixationStateList = [255 128 128; 128 255 128; 255 255 255];


global nSequences; nSequences = 6; % numbers of repeats
global NtoneStimuli 
global sequenceOrder

% this sets the ISI between tones in a sequence
global MinTimeBetweenTrials; MinTimeBetweenTrials = 1800;
global MaxTimeBetweenTrials; MaxTimeBetweenTrials = 2200;

% this sets the ISI between different sequences
global MinTimeBetweenSequences; MinTimeBetweenSequences = 3000;
global MaxTimeBetweenSequences; MaxTimeBetweenSequences = 3500;

%declare active triggers
active_triggers = [176:191,193:207];

%% get some subject scanning info
si = input('Enter Subject Initials: ','s');
blocknumber = input('Enter Block Number: ','s');

% check if data file already exists, and ask for block number again if
% needed
dat_name = [si blocknumber '_MEG_tones_passive.mat'];

if exist(dat_name,'file')==1
    disp('File name already in use')
end

% double check that it really is meant to be MEG or notMEG
try
    MEGSwitchCheck( IsMEGExpt ) % double check that the IsMEGExpt swith is set correctly
catch
    warning('Make sure the MEGExpt switch is set correctly') %#ok<WNTAG>
end


%% Loading sound samples
AudioData = h5info('lowrange095.h5','/');
samples.names = {AudioData.Groups.Groups.Datasets.Name};
velocity = 'v095'; %set the velocity of the tones...

samples.sounds = cellfun(@(x) ...
    h5read('lowrange095.h5',['/samples/',velocity,'/',x]), ...
    samples.names,'UniformOutput',false);

soundlist = cell2struct([samples.names;samples.sounds],{'name' 'sound'});

NtoneStimuli = length(soundlist);



%% make the trial list
% Triallist
%Column 1: Exemplar
%Column 2: NOT USED
%Column 3: NOT USED
%Column 4: Interstimulus time (ISI);
%Column 5: Stimulus onset time
%Column 6: Stimulus onset time (absolute)
%Column 7: Stimulus offset time (absolute)
%Column 8: NOT USED
%Column 9: NOT USED
%Column 10: Triggers

[TrialListNames,TrialList] = makeToneTriallist;
AllTheTrials = (1:nSequences*NtoneStimuli);


%% set up parallel port and trigger support (also, dont touch this)
% Configure the pins

if IsMEGExpt
    [ioObj,address] = MQinitPP;
      
    % find out how many trigger lines are needed and declare them
    ExptTriggers = active_triggers(1:length(soundlist)+1);
    %
    
    for i = 1:length(ExptTriggers-2)
        soundlist(i).triggerline = ExptTriggers(i);
    end
    
    soundlist(end).name = 'onset passive';

end

%% % setup the screens

   
    %% wait for keypress be begin

key = 0;
disp('Press a key to begin');

[choice] = MyGetKey(ValidResponses);


while key == 0
    [keyIsDown,secs,keyCode] = KbCheck;
    
    if keyIsDown
        key = 1;
    end
end

%% Clear the Screen / Prepare for trials
if blockScreen == 1
    Screen('FillRect',window,[BackCol BackCol BackCol],winRect)
    Screen('FillOval',window,[255 255 255],FixRect)

    Screen('Flip', window);
end
%%

ExperimentQuit = 0;

% begin the experiment
t0 = clock; % set the clocks and show the fixation
WaitSecs(1+rand*2);

if blockScreen == 1
    oldTextSize=Screen('TextSize', window, 12);
end

disp('playingsound')

for TrialCount = 1:length(TrialList)
    
    trialStart = clock;
       
   % play sound here
    PsychPortAudio('FillBuffer', AudioHandle, soundlist(TrialList(TrialCount,1)).sound');
    StimulusOnsetTime = PsychPortAudio('Start', AudioHandle, 1, 0, 0);
      
    if IsMEGExpt == 1
        MQsendtrigger(ioObj,address,soundlist(TrialList(TrialCount,1)).triggerline,.005);
    end
    
    TrialList(TrialCount,5) = StimulusOnsetTime;
    TrialList(TrialCount,6) = etime(trialStart,t0); % stimulus onset time from start of experiment
    TrialList(TrialCount,7) = etime(clock,trialStart);
    TrialList(TrialCount,8) = etime(clock,t0);
    % save the trigger that was sent
    
    if IsMEGExpt == 1 
        TrialList(TrialCount,10) = soundlist(TrialList(TrialCount,1)).triggerline;
    end
    
    WaitSecs(TrialList(TrialCount,4)/1000); % pause between trials
end


%% Display Statistic In0formation


sca

StimParam.pulse_width = pulse_width;
StimParam.FixSize = FixSize;
StimParam.gammaVals=gammaVals;
StimParam.StimDuration = StimDuration;
StimParam.ResponseTimeOut = ResponseTimeOut;

StimParam.NtrialsPerExemplar = nSequences;
StimParam.MinTimeBetweenTrials = MinTimeBetweenTrials;
StimParam.MaxTimeBetweenTrials = MaxTimeBetweenTrials;
StimParam.MaxTimeBetweenSequences = MaxTimeBetweenSequences;
StimParam.MinTimeBetweenSequences = MinTimeBetweenSequences;


%% Save Block Information

%dat_name = [si blocknumber '_MEG_tones_passive.mat'];

if exist(dat_name,'file')==0
    eval(['save ' dat_name ' StimParam TrialList TrialListNames soundlist sequenceOrder']);
else
    fprintf(1,'file already exists. Please save data manually\n');
    [filename, pathname] = uiputfile('matlab.mat', 'Save data as');
    eval(['save ' filename]);
end

ShowCursor
PsychPortAudio('Close')


endTimeMark = clock;