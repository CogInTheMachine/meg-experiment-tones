
INFO = h5info('i:\samples.h5_old');

for i = 1 : length(INFO.Groups.Groups(95).Datasets)
    chunk = h5read('i:\samples.h5_old',[INFO.Groups.Groups(95).Name,'/',...
        INFO.Groups.Groups(95).Datasets(i).Name]);
    samples.v095.(INFO.Groups.Groups(100).Datasets(i).Name) = ...
        repmat(chunk,1,2);
end

save('lowrange095.h5','samples');

