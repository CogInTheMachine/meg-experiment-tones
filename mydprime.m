function [anal] = mydprime(Responses,Answers);

% function to compute d rpime stats
% function assumes 1 = target present; 0 = target absent
% function assumes 1 = response target; 0 = no response;

TargetPresent = find(Answers == 1);
TargetAbsent = find(Answers == 0);
ResponsePresent = find(Responses == 1);
ResponseAbsent = find(Responses == 0);

HitRate = length(intersect(TargetPresent,ResponsePresent))/length(TargetPresent);
FARate  = length(intersect(TargetAbsent,ResponsePresent))/length(TargetAbsent);

MissRate = length(intersect(TargetPresent,ResponseAbsent))/length(TargetPresent);
CRRate  = length(intersect(TargetAbsent,ResponseAbsent))/length(TargetAbsent);

anal.hits = length(intersect(TargetPresent,ResponsePresent));
anal.correctRejects = length(intersect(TargetAbsent,ResponseAbsent));
anal.misses = length(intersect(TargetPresent,ResponseAbsent));
anal.falseAlarms = length(intersect(TargetAbsent,ResponsePresent));
anal.hitsPercent = anal.hits/length(TargetPresent);
anal.correctRejectsPercent = anal.correctRejects/length(TargetAbsent);
anal.missesPercent = anal.misses/length(TargetPresent);
anal.falseAlarmsPercent = anal.falseAlarms/length(TargetAbsent);

Hits = anal.hitsPercent;
FA = anal.falseAlarmsPercent;

%--Error checking
if anal.hitsPercent == 1 % if 100% Hits
% if e.g. 50 signal present trials Hits is between 98-100%
   Hits = 1-(1/length(TargetPresent));
    % N is the number of signal present trials
end


if anal.falseAlarmsPercent == 0 % if 0% FA
    % if e.g. 50 signal absent trials FA is between 0-2%
    FA = 1/length(TargetAbsent)
    % N is the number of signal absent trials
end

%-- Convert to Z scores
zHit = norminv(Hits/length(TargetPresent)) ;
zFA = norminv(FA/length(TargetAbsent)) ;

%-- Calculate d-prime
dPrime = zHit - zFA;
anal.dprime = dPrime;

end